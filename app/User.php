<?php

namespace App;

use App\Models\Address;
use App\Models\Post;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

//    protected $table = 'custom_users';
//
//    protected $primaryKey = 'custom_id';
//
//    const CREATED_AT = 'custom_create_field';
//
//    const UPDATED_AT =  'custom_updated_field';
//
//    public $timestamps = false;
//
//    protected $dates = [
//        'expired_at'
//    ];

        const USER = 1;
        const ADMIN = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function address()
    {
        return $this->hasOne(Address::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class,'post_author');
    }

    public function getWalletAttribute($value)
    {
        return number_format($value);
    }
    public static function getUserRoles()
    {
        return [
            self::USER => 'کاربر عادی',
            self::ADMIN => 'کاربر مدیر'
        ];
    }
}
