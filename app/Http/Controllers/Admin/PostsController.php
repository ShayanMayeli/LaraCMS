<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostCreateRequest;
use App\Models\Category;
use App\Models\Post;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::get();//Post::where('wallet','>',100000)->get();

        return view('admin.posts.list', compact('posts'));
    }

    public function create()
    {
        $postStatuses = Post::postStatuses();
        $allCategories = Category::get();
        $categories =  $allCategories->groupBy('parent_id');
        $categories['root'] = $categories[''];
        unset($categories['']);
        return $categories;
        return view('admin.posts.create', compact('postStatuses','categories'));
    }

    public function store(PostCreateRequest $request)
    {

        $post = Post::create([
            'post_title'   => $request->input('postTitle'),
            'post_slug'    => $request->input('postTitle'),
            'post_content' => $request->input('postContent'),
            'post_author'  => User::first()->id, //Auth::id(),
            'post_status'  => $request->input('postStatus')
        ]);
        if ($post && $post instanceof Post) {
            $categories = $request->input('categories');
            if(count($categories) > 0)
            {
                $post->categories()->attach($categories);
            }
            return back()->with('status', 'مطلب جدید با موفقیت ایجاد گردید!');
        }
    }

    public function delete(Request $request, $post_id)
    {

        $post         = Post::find($post_id);
        $deleteResult = $post->delete();
        if ($deleteResult) {
            return back()->with('status', 'مطلب با موفقیت حذف گردید');
        }
//        Post::destroy($post_id);
    }

    public function edit(Request $request, $post_id)
    {
        $post      = Post::find($post_id);
        $categories = Category::get();
        $postCategories = $post->categories->pluck('id')->toArray();
        $postStatuses = Post::postStatuses();
        return view('admin.posts.edit', compact('post', 'postStatuses','postCategories','categories'));

    }

    public function update(Request $request, $post_id)
    {
        $post = Post::find($post_id);
        if ($post && $post instanceof Post) {
            $postData = [
                'post_title'  => $request->input('postTitle'),
                'post_slug'   => $request->input('postTitle'),
                'post_content' => $request->input('postContent'),
                'post_status'   => $request->input('postStatus')
            ];

            $updateResult = $post->update($postData);
            if ($updateResult) {
                $categories = $request->input('categories');
                if(count($categories) > 0)
                {
                    $post->categories()->sync($categories);
                }
                return redirect()->route('admin.posts')->with('status', 'مطلب با موفقیت به روز رسانی گردید!');
            }
        }
    }
}
