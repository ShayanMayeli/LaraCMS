<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UsersController@index')->name('admin.users');
        Route::get('create', 'UsersController@create')->name('admin.users.create');
        Route::post('store', 'UsersController@store')->name('admin.users.store');
        Route::get('delete/{user_id}', 'UsersController@delete')->name('admin.users.delete');
        Route::get('edit/{user_id}', 'UsersController@edit')->name('admin.users.edit');
        Route::post('update/{user_id}', 'UsersController@update')->name('admin.users.update');

    });
    Route::group(['prefix' => 'posts'],function(){

        Route::get('/', 'PostsController@index')->name('admin.posts');
        Route::get('create', 'PostsController@create')->name('admin.posts.create');
        Route::post('store', 'PostsController@store')->name('admin.posts.store');
        Route::get('delete/{post_id}', 'PostsController@delete')->name('admin.posts.delete');
        Route::get('edit/{post_id}', 'PostsController@edit')->name('admin.posts.edit');
        Route::post('update/{post_id}', 'PostsController@update')->name('admin.posts.update');
        
    });
});

